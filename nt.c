#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#define VALUE_MAX_STR_LEN 64
#define bool int
#define true 1
#define false 0


struct value_st {
    char arg[VALUE_MAX_STR_LEN];
    unsigned int normalized;
    char bin_str_input[VALUE_MAX_STR_LEN];
    char bin_str_literal[VALUE_MAX_STR_LEN];
    char bin_str_human[VALUE_MAX_STR_LEN];
    char hex_str[VALUE_MAX_STR_LEN];
    char hex_str_input[VALUE_MAX_STR_LEN];
    char dec_str_unsigned[VALUE_MAX_STR_LEN];
    char dec_str_signed[VALUE_MAX_STR_LEN];
    char *bit_width;
    char *bit_range_low;
    char *bit_range_high;
    bool input_negative;
};


int dec_str_to_unsigned(struct value_st *v_st_p)
{
    int i;
    int len;
    int d;
    unsigned int x = 0;
    int factor = 10;

    len = strlen(v_st_p->arg);

    for (i = 0; i < len; i++) {
        x = x * factor;
        d = v_st_p->arg[i] - '0';
        x = x + d;
    }

    v_st_p->normalized = x;

    return 0;
}


int unsigned_to_binary_literal(struct value_st *v_st_p)
{
    int si; int i;
    unsigned int x;
    v_st_p->bin_str_literal[0] = '0';
    v_st_p->bin_str_literal[1] = 'b';

    si = 1;
//    si = si + 32;
    si = si + atoi(v_st_p->bit_width);

    x = v_st_p->normalized;

    for (i = 0; i < atoi(v_st_p->bit_width); i++) {
        /*
        b = x & 0b1;
        if (b == 0) {
            bchar = '0';
        } else {
            bchar = '1';
        }
        v_st_p->bin_str_literal[si] = bchar;
        */

        v_st_p->bin_str_literal[si] = (x & 0b1) ? '1' : '0';
        x = x >> 1;
        si = si - 1;
    }

    if (v_st_p->input_negative == true){
        for (int j = 2; j < strlen(v_st_p->bin_str_literal); ++j) {
            if(v_st_p->bin_str_literal[j] == '0'){
                v_st_p->bin_str_literal[j] = '1';
            }else{
                v_st_p->bin_str_literal[j] = '0';
            }
        }

        if (v_st_p->bin_str_literal[strlen(v_st_p->bin_str_literal)-1] == '1'){
            for (int j = 2; j < strlen(v_st_p->bin_str_literal); ++j) {
                if (v_st_p->bin_str_literal[strlen(v_st_p->bin_str_literal)-1-j-2] == '0'){
                    v_st_p->bin_str_literal[strlen(v_st_p->bin_str_literal)-1-j-2] = '1';
                    break;
                } else{
                    v_st_p->bin_str_literal[strlen(v_st_p->bin_str_literal)-1-j-2] = '0';
                }
            }
        }else{
            v_st_p->bin_str_literal[strlen(v_st_p->bin_str_literal)-1] = '1';
        }
    }

    v_st_p->bin_str_literal[2 + atoi(v_st_p->bit_width)] = '\0';

    return 0;
}


int unsigned_to_binary_human(struct value_st *v_st_p)
{
    int si; int i;
    unsigned int x;
    int b; char bchar;

    si = atoi(v_st_p->bit_width)-1;

    x = v_st_p->normalized;

    for (i = 0; i < atoi(v_st_p->bit_width); i++) {

        b = x & 0b1;
        if (b == 0) {
            bchar = '0';
        } else {
            bchar = '1';
        }
        v_st_p->bin_str_human[si] = bchar;

        x = x >> 1;
        si = si - 1;
    }

    if (v_st_p->input_negative == true){
        for (int j = 0; j < strlen(v_st_p->bin_str_human); ++j) {
            if(v_st_p->bin_str_human[j] == '0'){
                v_st_p->bin_str_human[j] = '1';
            }else{
                v_st_p->bin_str_human[j] = '0';
            }
        }

        if (v_st_p->bin_str_human[strlen(v_st_p->bin_str_human)-1] == '1'){
            for (int j = 0; j < strlen(v_st_p->bin_str_human); ++j) {
                if (v_st_p->bin_str_human[strlen(v_st_p->bin_str_human)-1-j] == '0'){
                    v_st_p->bin_str_human[strlen(v_st_p->bin_str_human)-1-j] = '1';
                    break;
                } else{
                    v_st_p->bin_str_human[strlen(v_st_p->bin_str_human)-1-j] = '0';
                }
            }
        }else{
            v_st_p->bin_str_human[strlen(v_st_p->bin_str_human)-1] = '1';
        }
    }

    v_st_p->bin_str_human[atoi(v_st_p->bit_width)] = '\0';

    return 0;
}


int binary_to_hexadecimal(struct value_st *v_st_p)
{
    char temp[VALUE_MAX_STR_LEN];

    if ( strncmp(v_st_p->bin_str_input, "", 2) != 0){

        if (v_st_p->bit_range_low != NULL && v_st_p->bit_range_high != NULL){
            for (int i = 2; i < 2+v_st_p->bit_range_high-v_st_p->bit_range_low+1+1; ++i) {
                temp[i-2] = v_st_p->bin_str_input[i-2+strlen(v_st_p->bin_str_input)-1-(atoi(v_st_p->bit_range_high))];
            }
        }else{
            for (int i = 2; i < strlen(v_st_p->bin_str_input); ++i) {
                temp[i-2] = v_st_p->bin_str_input[i];
            }
        }

    } else{
        for (int i = 2; i <strlen(v_st_p->bin_str_literal) ; ++i) {
            temp[i-2] = v_st_p->bin_str_literal[i];
        }
    }

    int hexDigitToBinary[16] = {0, 1, 10, 11, 100, 101, 110, 111, 1000,
                                1001, 1010, 1011, 1100, 1101, 1110, 1111};
    char hexDigits[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
                          '9', 'A', 'B', 'C', 'D', 'E', 'F'};

//    char *hexadecimal = malloc(VALUE_MAX_STR_LEN);
    char hexadecimal[VALUE_MAX_STR_LEN];
    int digit = 0;
    int i;
    int digitsOf4 = 0;
    int factor = 1;

    for (int k = 0; k < strlen(temp); ++k) {
        int tmp = (temp[strlen(temp)-1-k]-'0') * factor;
        digitsOf4 += tmp;
        factor *= 10;

        if (k%4 == 3){
            for(i=0; i<16; i++) {
                if(hexDigitToBinary[i] == digitsOf4) {
                    hexadecimal[digit] = hexDigits[i];
                    digit++;
                    break;
                }
            }
            digitsOf4 = 0;
            factor = 1;
        }
    }

    v_st_p->hex_str[0] = '0';
    v_st_p->hex_str[1] = 'x';

    int digits_after0x = (atoi(v_st_p->bit_width)/4);
    int digits_all = digits_after0x + 2;

    for (int j = 2; j < digits_all; ++j) {
        if (hexadecimal[digits_all-j-1]){
            v_st_p->hex_str[j] = hexadecimal[digits_all-j-1];
        } else{
            v_st_p->hex_str[j] = '0';
        }
    }

    v_st_p->hex_str[digits_all] = '\0';

    return 0;
}


int hexadecimal_to_unsigned_decimal(struct value_st *v_st_p){
    char hexDigits[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
                          '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    char temp[VALUE_MAX_STR_LEN];

    if (strncmp(v_st_p->hex_str, "", 2) == 0){
        strncpy(temp, v_st_p->hex_str_input, VALUE_MAX_STR_LEN - 1);
    } else{
        strncpy(temp, v_st_p->hex_str, VALUE_MAX_STR_LEN - 1);
    }

    long long decimal=0;
    int i, j, power=0;

    for(i = strlen(temp)-1; i >= 2; i--) {

        for(j=0; j<16; j++){

            if(temp[i] == hexDigits[j]){
                decimal += j*pow(16, power);
            }
        }
        power++;
    }

    sprintf(v_st_p->dec_str_unsigned, "%lld", decimal);

    return 0;

}

int binary_to_signed_decimal(struct value_st *v_st_p) {
    int remainder;
    int decimal = 0;
    long i=0;

    char temp[VALUE_MAX_STR_LEN];

    if (strncmp(v_st_p->bin_str_input, "", 2) != 0 && v_st_p->bin_str_input[2] == '1' && v_st_p->bit_range_low == NULL &&v_st_p->bit_range_high == NULL){
        for (int j = 2; j < strlen(v_st_p->bin_str_input); ++j) {
            temp[j-2] = v_st_p->bin_str_input[j];
        }
        temp[strlen(v_st_p->bin_str_input)-2] = '\0';
    } else{
        strncpy(temp, v_st_p->bin_str_human, VALUE_MAX_STR_LEN - 1);
    }

//    strncpy(temp, v_st_p->bin_str_human, VALUE_MAX_STR_LEN - 1);

    bool input_neg = false;

    if(temp[0] == '1'){
        input_neg = true;
        for (int j = 0; j < strlen(temp); ++j) {
            if(temp[j] == '0'){
                temp[j] = '1';
            }else{
                temp[j] = '0';
            }
        }

        if (temp[strlen(temp)-1] == '1'){
            for (int j = 0; j < strlen(temp); ++j) {
                if (temp[strlen(temp)-1-j] == '0'){
                    temp[strlen(temp)-1-j] = '1';
                    break;
                } else{
                    temp[strlen(temp)-1-j] = '0';
                }
            }
        }else{
            temp[strlen(temp)-1] = '1';
        }
    }

    long the_bin = atoi(temp);

    while(the_bin != 0) {
        remainder = the_bin%10;
        the_bin = the_bin/10;
        decimal += (remainder * pow(2,i));
        ++i;
    }

    if (input_neg == true){
        sprintf(v_st_p->dec_str_signed, "%d", 0-decimal);
    } else{
        sprintf(v_st_p->dec_str_signed, "%d", decimal);
    }

    return 0;
}


//char *reverseStr(char *str){
//    char temp;
//    int i, j = 0;
//    i = 0;
//    j = strlen(str) - 1;
//
//    while (i < j) {
//        temp = str[i];
//        str[i] = str[j];
//        str[j] = temp;
//        i++;
//        j--;
//    }
//
//    return str;
//}


void print_converted_values(struct value_st *v_st_p) {
//    printf("x = %d\n", v_st_p->normalized);

    int bits = strlen(v_st_p->bin_str_human);

    for (int i = 0; i < bits; ++i) {

        if ((i%4 == 0) & (i>0) & (v_st_p->bin_str_human[i] != '\0')){
            printf(" ");
        }
        printf("%c", v_st_p->bin_str_human[i]);
    }
    printf(" (base 2)\n");

    printf("%s (base 2)\n", v_st_p->bin_str_literal);
    printf("%s (base 16)\n", v_st_p->hex_str);
    printf("%s (base 10 unsigned)\n", v_st_p->dec_str_unsigned);
    printf("%s (base 10 signed)\n", v_st_p->dec_str_signed);
    return;
}


int main(int argc, char *argv[])
{
    struct value_st v_st;

    if (argc <= 1){
        printf("invalid input\n");
        printf("should be something like {$nt 713}\n");
        exit(-1);
    }

    for (int j = 0; j < argc; ++j) {
        if (strncmp(argv[j], "-b", 2) == 0){
            int value = atoi(argv[j+1]);
            if (value > 32 || value<=0 || value%4!=0 || isdigit(value)) {
                printf("-b invalid input\n");
                printf("should be something like {-b 16}, the value has to be within 0 to 32, and able to divided by 4.\n");
                return 0;
            }
            v_st.bit_width = argv[j+1];
        }else if (strncmp(argv[j], "-r", 2) == 0){

            if (strstr(argv[j+1], ",") == NULL){
                printf("-r invalid input\n");
                printf("should be something like {-r 4, 7}.\n");
                return 0;
            }

            v_st.bit_range_low = strtok(argv[j+1], ",");
            v_st.bit_range_high = strtok(NULL, ",");

            int value_low = atoi(v_st.bit_range_low);
            int value_high = atoi(v_st.bit_range_high);
            if (value_high > 32 || value_low<=0 || isdigit(value_low) || isdigit(value_high) || value_high<value_low) {
                printf("-r invalid input\n");
                printf("should be something like {-r 4, 7}, the value has to be within 0 to 32, and always lower first.\n");
                return 0;
            }
        }
    }

    if (v_st.bit_width == NULL){
        v_st.bit_width = "32";
    }

    if (argv[argc-1][0] == '0' && argv[argc-1][1] == 'b'){
        if (strlen(argv[argc-1]) > 34){
            printf("binary input exceed 32 bits\n");
            exit(-1);
        }
        strncpy(v_st.bin_str_input, argv[argc-1], VALUE_MAX_STR_LEN - 1);

        if (binary_to_hexadecimal(&v_st) < 0) {
            printf("binary_to_hexadecimal() failed\n");
            exit(-1);
        }

        if (hexadecimal_to_unsigned_decimal(&v_st) < 0) {
            printf("hexadecimal_to_unsigned_decimal() failed\n");
            exit(-1);
        }
        strncpy(v_st.arg, v_st.dec_str_unsigned, VALUE_MAX_STR_LEN - 1);

        if (dec_str_to_unsigned(&v_st) < 0) {
            printf("dec_str_to_unsigned() failed\n");
            exit(-1);
        }

        if (unsigned_to_binary_literal(&v_st) < 0) {
            printf("unsigned_to_binary_literal() failed\n");
            exit(-1);
        }

        if (unsigned_to_binary_human(&v_st) < 0) {
            printf("unsigned_to_binary_human() failed\n");
            exit(-1);
        }

        if (binary_to_signed_decimal(&v_st) < 0) {
            printf("binary_to_signed_decimal() failed\n");
            exit(-1);
        }
    }else if (argv[argc-1][0] == '0' && argv[argc-1][1] == 'x'){
        if (strlen(argv[argc-1]) > 10){
            printf("hexadecimal input exceed 32 bits\n");
            exit(-1);
        }

        strncpy(v_st.hex_str_input, argv[argc-1], VALUE_MAX_STR_LEN - 1);

        if (hexadecimal_to_unsigned_decimal(&v_st) < 0) {
            printf("hexadecimal_to_unsigned_decimal() failed\n");
            exit(-1);
        }

        strncpy(v_st.arg, v_st.dec_str_unsigned, VALUE_MAX_STR_LEN - 1);

        if (dec_str_to_unsigned(&v_st) < 0) {
            printf("dec_str_to_unsigned() failed\n");
            exit(-1);
        }

        if (unsigned_to_binary_literal(&v_st) < 0) {
            printf("unsigned_to_binary_literal() failed\n");
            exit(-1);
        }

        if (unsigned_to_binary_human(&v_st) < 0) {
            printf("unsigned_to_binary_human() failed\n");
            exit(-1);
        }

        if (binary_to_signed_decimal(&v_st) < 0) {
            printf("binary_to_signed_decimal() failed\n");
            exit(-1);
        }

        if (binary_to_hexadecimal(&v_st) < 0) {
            printf("binary_to_hexadecimal() failed\n");
            exit(-1);
        }

    }else if(argv[argc-1][0] == '-'){
        if(strlen(argv[argc-1]) > 33){
            printf("input exceed 32 bits\n");
            exit(-1);
        }
        v_st.input_negative = true;

        char temp[VALUE_MAX_STR_LEN];
        for (int i = 1; i < strlen(argv[argc-1]); ++i) {
            temp[i-1] = argv[argc-1][i];
        }

        strncpy(v_st.arg, temp, VALUE_MAX_STR_LEN - 1);

        if (dec_str_to_unsigned(&v_st) < 0) {
            printf("dec_str_to_unsigned() failed\n");
            exit(-1);
        }

        if (unsigned_to_binary_literal(&v_st) < 0) {
            printf("unsigned_to_binary_literal() failed\n");
            exit(-1);
        }

        if (unsigned_to_binary_human(&v_st) < 0) {
            printf("unsigned_to_binary_human() failed\n");
            exit(-1);
        }

        if (binary_to_signed_decimal(&v_st) < 0) {
            printf("binary_to_signed_decimal() failed\n");
            exit(-1);
        }

        if (binary_to_hexadecimal(&v_st) < 0) {
            printf("binary_to_hexadecimal() failed\n");
            exit(-1);
        }

        if (hexadecimal_to_unsigned_decimal(&v_st) < 0) {
            printf("hexadecimal_to_unsigned_decimal() failed\n");
            exit(-1);
        }

    } else {
        if(strlen(argv[argc-1]) > 32){
            printf("input exceed 32 bits\n");
            exit(-1);
        }

        strncpy(v_st.arg, argv[argc-1], VALUE_MAX_STR_LEN - 1);

        if (dec_str_to_unsigned(&v_st) < 0) {
            printf("dec_str_to_unsigned() failed\n");
            exit(-1);
        }

        if (unsigned_to_binary_literal(&v_st) < 0) {
            printf("unsigned_to_binary_literal() failed\n");
            exit(-1);
        }

        if (unsigned_to_binary_human(&v_st) < 0) {
            printf("unsigned_to_binary_human() failed\n");
            exit(-1);
        }

        if (binary_to_signed_decimal(&v_st) < 0) {
            printf("binary_to_signed_decimal() failed\n");
            exit(-1);
        }

        if (binary_to_hexadecimal(&v_st) < 0) {
            printf("binary_to_hexadecimal() failed\n");
            exit(-1);
        }

        if (hexadecimal_to_unsigned_decimal(&v_st) < 0) {
            printf("hexadecimal_to_unsigned_decimal() failed\n");
            exit(-1);
        }
    }

    print_converted_values(&v_st);
    return 0;
}

//long decimalToBinary(int n) {
//    int remainder;
//    long binary = 0;
//    long i = 1;
//
//    while(n != 0) {
//        remainder = n%2;
//        n = n/2;
//        binary += (remainder*i);
//        i = i*10;
//    }
//    return binary;
//}
